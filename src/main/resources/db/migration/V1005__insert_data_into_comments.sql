use forum;

insert into comments (text, user_id, theme_id, comment_ldt) values ('Some comment text1',1, 3, NOW()),
                                                      ('Some comment text2',1, 2, NOW()),
                                                      ('Some comment text3',2, 3, NOW()),
                                                      ('Some comment text4',3, 3, NOW()),
                                                      ('Some comment text5',3, 2, NOW()),
                                                      ('Some comment text6',4, 2, NOW()),
                                                      ('Some comment text7',4, 1, NOW()),
                                                      ('Some comment text8',2, 3, NOW()),
                                                      ('Some comment text9',1, 4, NOW());