use forum;

insert  into   users (first_name,last_name,username,password,gender,enabled,role) values
                        ('nikita', 'nikitov', 'nikita1','pass','MALE',1,'USER');
insert  into   users (first_name,last_name,username,password,gender,enabled,role) values
                        ('nikita2', 'nikitov2', 'nikita2','pass2','MALE',1,'USER');
insert  into   users (first_name,last_name,username,password,gender,enabled,role) values
                        ('nikita3', 'nikitov3', 'nikita3','pass3','MALE',1,'USER');
insert  into   users (first_name,last_name,username,password,gender,enabled,role) values
                        ('nikita4', 'nikitov4', 'nikita4','pass4','MALE',1,'USER');
insert  into   users (first_name,last_name,username,password,gender,enabled,role) values
                        ('nikita5', 'nikitov5', 'nikita5','pass5','MALE',1,'USER');


insert into themes (title, user_id, theme_ldt) values ('Java',1, NOW()),
                                              ('Python',1, NOW()),
                                              ('C++',2, NOW()),
                                              ('C#',2, NOW()),
                                              ('JavaScript',2, NOW()),
                                              ('Data science',3, NOW()),
                                              ('SQL',3, NOW()),
                                              ('Oracle',3, NOW()),
                                              ('Microsoft',3, NOW());