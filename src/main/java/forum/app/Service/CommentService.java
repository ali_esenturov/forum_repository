package forum.app.Service;

import forum.app.Model.Comment;
import forum.app.Model.CommentDto;
import forum.app.Repository.CommentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public Page<CommentDto> getComments(Integer theme_id, Pageable pageable){
        return commentRepository.findAllByThemeIdOrderByTimeOfCommentAsc(theme_id, pageable).map(CommentDto::from);
    }

    public void saveComment(Comment comment){
        commentRepository.save(comment);
    }
}